package controllers

import (
	"app/dbmanager"
	apphttp "app/http"
	"app/models/db"
	"app/models/form"
	"app/services/mailer"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

const appName = "app"

var SessionManager = NewSessionManager()

type Session struct {
	UserId int64
	mux    *sync.RWMutex
}

type sessionManager struct {
	store map[string]*Session
	mux   *sync.RWMutex
}

func NewSessionManager() *sessionManager {
	return &sessionManager{
		store: make(map[string]*Session),
		mux:   &sync.RWMutex{},
	}
}

func (sm *sessionManager) Get(req *http.Request) (*Session, error) {
	sm.mux.RLock()
	defer sm.mux.RUnlock()
	cookie, err := req.Cookie(appName)
	if err != nil {
		return nil, err
	}

	if session, ok := sm.store[cookie.Value]; ok {
		return session, nil
	}
	return nil, fmt.Errorf("Not logged in")
}

func (sm *sessionManager) Add(w http.ResponseWriter, userId int64) {
	sm.mux.Lock()
	defer sm.mux.Unlock()
	// set cookie
	cookie, err := uuid()
	if err != nil {
		log.Println(w, "service unavailable, try again later")
		return
	}
	sm.store[cookie] = &Session{userId, &sync.RWMutex{}}
	http.SetCookie(w, &http.Cookie{Name: appName, Value: cookie, Expires: time.Now().Add(time.Hour)})
}

func (sm *sessionManager) Remove(req *http.Request) {
	sm.mux.Lock()
	defer sm.mux.Unlock()

	// we don't check for err because the we already did that in CheckAuth()
	cookie, _ := req.Cookie(appName)

	delete(sm.store, cookie.Value)
}

// uuid generates an uuid
func uuid() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	uuid := fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return uuid, err
}

// checkAuth checks if a request is authenticated before calling its handler
func CheckAuth(fn apphttp.AppHandler) apphttp.AppHandler {
	return apphttp.AppHandler(func(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
		_, err := SessionManager.Get(req)
		if err != nil {
			return &apphttp.AppError{err, "Not logged in", http.StatusUnauthorized}
		}
		fn(w, req)
		return nil
	})
}

// SignUp creates a new user by inserting its email and password in the database
func SignUp(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var user db.User
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		return &apphttp.AppError{err, "Error decoding request", 409}
	}

	// we set a random uuid to make sure that somebody will not reset the password
	user.ResetToken, _ = uuid()
	response, err := db.CreateNewUser(user)
	if err != nil {
		return &apphttp.AppError{err, "Cound not sign you up!", http.StatusInternalServerError}
	}

	// everything worked correct so we create a session and return the result
	SessionManager.Add(w, response.ID)

	json.NewEncoder(w).Encode(response)
	return nil
}

// SignIn validates credentials and opens the profile
func SignIn(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var user db.User
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		return &apphttp.AppError{err, "", http.StatusBadRequest}
	}

	// we try to get the id of the user so we can use it for other requests
	query := fmt.Sprintf("SELECT id, email FROM user WHERE email = '%s' AND password = '%s';", user.Email, user.Password)
	rows, err := dbmanager.GetDB().Query(query)
	if err != nil {
		// TODO check what kind of error that is and return a better error message/status
		return &apphttp.AppError{err, "Could not find the appropiate user", http.StatusUnauthorized}
	}

	response := db.User{}
	rows.Next()
	err = rows.Scan(&response.ID, &response.Email)
	if err != nil {
		// TODO check what kind of error that is and return a better error message/status
		return &apphttp.AppError{err, "Could not find the appropiate user", http.StatusUnauthorized}
	}

	// everything worked correct so we create a cookie and return the result
	SessionManager.Add(w, response.ID)

	json.NewEncoder(w).Encode(response)
	return nil
}

// Signout logs out a logged user
func SignOut(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	session, err := SessionManager.Get(req)
	if err != nil {
		return &apphttp.AppError{err, "Could not find the appropiate user", http.StatusUnauthorized}
	}

	accessToken, err := db.GetUserAccessToken(session.UserId)
	if err != nil {
		// this user doesn't have an access_token
	} else {
		// Execute HTTP GET request to revoke current token
		url := "https://accounts.google.com/o/oauth2/revoke?token=" + accessToken
		resp, err := http.Get(url)
		if err != nil {
			m := "Failed to revoke token for a given user"
			return &apphttp.AppError{err, m, 400}
		}
		defer resp.Body.Close()
	}

	// we remove the cookie to force the user to sign in next time
	SessionManager.Remove(req)
	return nil
}

// ForgotPassword generates a new token and sends a reset link to user
func ForgotPassword(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var fp *form.ForgotPassword
	err := json.NewDecoder(req.Body).Decode(&fp)
	if err != nil {
		return &apphttp.AppError{err, "", http.StatusBadRequest}
	}

	// generate a new token
	resetToken, _ := uuid()
	// store the reset token in the database
	err = db.InsertResetToken(fp.Email, resetToken)
	if err != nil {
		return &apphttp.AppError{err, "Could not send you a reset token", http.StatusInternalServerError}
	}

	// TODO use a better way of getting the hostname
	resetLink := fmt.Sprintf("http://ec2-13-58-117-141.us-east-2.compute.amazonaws.com:4567/static/public/resetpassword.html?email=%s&token=%s", fp.Email, resetToken)
	msg := fmt.Sprintf("To: %s\r\n"+
		"Subject: Password reset link!\r\n"+
		"\r\n%s\r\n", fp.Email, resetLink)
	// send an email with the reset link to the specified email address
	err = mailer.SendMail(fp.Email, msg)
	if err != nil {
		return &apphttp.AppError{err, "Could not send you a reset token", http.StatusInternalServerError}
	}
	return nil
}

// ResetPassword checks the email/token and sets the new password for this user
func ResetPassword(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var rp *form.ResetPassword
	err := json.NewDecoder(req.Body).Decode(&rp)
	if err != nil {
		return &apphttp.AppError{err, "", http.StatusBadRequest}
	}

	if len(rp.Password) < 1 {
		return &apphttp.AppError{err, "Could not reset your password", http.StatusInternalServerError}
	}
	resetToken, _ := uuid()
	err = db.ResetPassword(rp, resetToken)
	if err != nil {
		return &apphttp.AppError{err, "Could not reset your password", http.StatusInternalServerError}
	}
	return nil
}
