package controllers

import (
	apphttp "app/http"
	"app/models/db"
	"app/models/form"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Update your Google API project information here.
const (
	clientID     = "xxxxxxxxxxxxxxxxx.apps.googleusercontent.com"
	clientSecret = "xxxxxxxxxxxxxxxxx"
)

// config is the configuration specification supplied to the OAuth package.
var conf = &oauth2.Config{
	ClientID:     clientID,
	ClientSecret: clientSecret,
	RedirectURL:  "postmessage",
	Scopes: []string{
		"https://www.googleapis.com/auth/userinfo.email", // You have to select your own scope from here -> https://developers.google.com/identity/protocols/googlescopes#google_sign-in
	},
	Endpoint: google.Endpoint,
}

// User is a retrieved and authentiacted user.
type GoogleUser struct {
	ID         string `json:"id"`
	Email      string `json:"email"`
	Name       string `json:"name"`
	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	Link       string `json:"link"`
	Picture    string `json:"picture"`
	Gender     string `json:"gender"`
	Locale     string `json:"locale"`
}

func GoogleLogin(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var googleSignIn form.GoogleSignIn
	err := json.NewDecoder(req.Body).Decode(&googleSignIn)
	if err != nil {
		return &apphttp.AppError{err, "Error decoding request", 409}
	}

	// TODO check the idToken
	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + googleSignIn.AccessToken)

	defer response.Body.Close()
	data, _ := ioutil.ReadAll(response.Body)

	var googleUser GoogleUser
	err = json.Unmarshal(data, &googleUser)
	if err != nil {
		return &apphttp.AppError{err, "Error decoding request", http.StatusInternalServerError}
	}

	// check if the user already exists
	result, _ := db.GetUser(googleUser.Email)
	if result != nil {
		// update the users profile with data collected from google
		result.FullName = googleUser.Name
		result.AccessToken = googleSignIn.AccessToken

		err := result.Save()
		if err != nil {
			return &apphttp.AppError{err, "Cound not sign you in!", http.StatusInternalServerError}
		}
	} else {
		var newUser db.User

		// fill the newUser profile with data collected from google
		newUser.Email = googleUser.Email
		newUser.FullName = googleUser.Name
		newUser.AccessToken = googleSignIn.AccessToken

		var err error
		result, err = db.CreateNewUser(newUser)
		if err != nil {
			return &apphttp.AppError{err, "Cound not sign you in!", http.StatusInternalServerError}
		}
	}

	// everything worked correct so we create a cookie and return the result
	SessionManager.Add(w, result.ID)

	json.NewEncoder(w).Encode(result)
	return nil
}
