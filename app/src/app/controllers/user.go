package controllers

import (
	"app/dbmanager"
	apphttp "app/http"
	"app/models/db"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// UserHandler redirects each request to its propper handler
func UserHandler(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	switch req.Method {
	case "GET":
		// Serve the resource.
	case "POST":
		// Create a new record.
		return GetUserEndpoint(w, req)
	case "PUT":
		// Update an existing record.
		return UpdateUserEndPoint(w, req)
	case "DELETE":
		// Remove the record.
	default:
		// Give an error message.
	}
	return &apphttp.AppError{nil, "Error finding appropiate method", 404}
}

// GetUserEndpoint returns profile information about the user based on user's id
func GetUserEndpoint(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var user db.User
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		log.Println(err)
	}

	// get most of the users information
	query := fmt.Sprintf("SELECT fullname, address, email, telephone FROM user WHERE id = %d;", user.ID)
	rows, err := dbmanager.GetDB().Query(query)
	if err != nil {
		// TODO check what kind of error that is and return a better error message/status
		return &apphttp.AppError{err, "Error finding requested user", 404}
	}

	// extract the user's info from query response
	rows.Next()
	err = rows.Scan(&user.FullName, &user.Address, &user.Email, &user.Telephone)
	if err != nil {
		// TODO check what kind of error that is and return a better error message/status
		return &apphttp.AppError{err, "Error finding requested user", 404}
	}

	// serialize the user object in json fomat and send it back
	json.NewEncoder(w).Encode(user)
	return nil
}

// UpdateUserEndPoint handles the updates in user's profile information
func UpdateUserEndPoint(w http.ResponseWriter, req *http.Request) *apphttp.AppError {
	var user db.User
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		log.Println(err)
	}

	err = user.Save()
	if err != nil {
		return &apphttp.AppError{err, "Error updating user's information", 500}
	}
	// serialize the user object in json fomat and send it back
	json.NewEncoder(w).Encode(user)
	return nil
}
