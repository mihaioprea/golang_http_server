package main

import (
	"app/controllers"
	"app/dbmanager"
	apphttp "app/http"
	"app/services/mailer"
	"fmt"
	"log"
	"net/http"
)

func main() {
	// TODO use flags or a configuration file to get the credentials
	databaseAddress := "tcp(localhost:3306)"
	databaseUsername := "xxxxxxxxxx"
	databasePassword := "xxxxxxxxxx"

	smtpServerAddress := "localhost:25"

	// prepare the sqlDSN for the sql client
	sqlDSN := fmt.Sprintf("%s:%s@%s/", databaseUsername, databasePassword, databaseAddress)
	db, err := dbmanager.InitDb(sqlDSN)
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Println("Connected to the database!")
	defer db.Close()

	// setup the default Mailer
	mailer.InitDefaultMailer(smtpServerAddress, "test@demo.com")

	// Register handlers for our API calls
	http.Handle("/googleSignIn", apphttp.AppHandler(controllers.GoogleLogin))
	http.Handle("/signup", apphttp.AppHandler(controllers.SignUp))
	http.Handle("/signin", apphttp.AppHandler(controllers.SignIn))
	http.Handle("/signout", controllers.CheckAuth(controllers.SignOut))
	http.Handle("/forgotpassword", apphttp.AppHandler(controllers.ForgotPassword))
	http.Handle("/resetpassword", apphttp.AppHandler(controllers.ResetPassword))

	http.Handle("/user", apphttp.AppHandler(controllers.CheckAuth(controllers.UserHandler)))

	http.Handle("/static/private/", controllers.CheckAuth(func(w http.ResponseWriter, r *http.Request) *apphttp.AppError {
		http.ServeFile(w, r, r.URL.Path[1:])
		return nil
	}))

	http.HandleFunc("/static/public/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	log.Println("HTTP server started!")
	err = http.ListenAndServe(":4567", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
