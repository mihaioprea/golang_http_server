package form

// GoogleSignIn is the form sent by a user in order to login
type GoogleSignIn struct {
	AccessToken string `json:"access_token"`
	IdToken     string `json:"id_token"`
}
