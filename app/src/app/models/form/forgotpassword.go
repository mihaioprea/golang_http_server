package form

// ForgotPassword is the form sent by a user in order to request a password change
type ForgotPassword struct {
	Email string `json:"email"`
}

// ResetPassword is the form sent by a user in order to change it's password
type ResetPassword struct {
	Email      string `json:"email"`
	ResetToken string `json:"reset_token"`
	Password   string `json:"password"`
}
