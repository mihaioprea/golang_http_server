package db

import (
	"app/dbmanager"
	"app/models/form"
	"fmt"
)

type User struct {
	ID          int64  `json:"id,omitempty"`
	FullName    string `json:"fullname,omitempty"`
	Address     string `json:"address,omitempty"`
	Email       string `json:"email,omitempty"`
	Telephone   string `json:"telephone,omitempty"`
	Password    string `json:"password,omitempty"`
	AccessToken string `json:"access_token,omitempty"`
	ResetToken  string `json:"reset_token,omitempty"`
}

// Save is updating information related fields of the user
func (u *User) Save() error {
	query := fmt.Sprintf("UPDATE user SET fullname='%s', address='%s', email='%s', telephone='%s', access_token='%s' WHERE id=%d", u.FullName, u.Address, u.Email, u.Telephone, u.AccessToken, u.ID)
	_, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return err
	}
	return nil
}

//GetUserAccessToken returns a string with the google access_token
func GetUserAccessToken(userId int64) (string, error) {
	query := fmt.Sprintf("SELECT access_token FROM user WHERE id = %d;", userId)
	rows, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return "", err
	}

	// extract the user's info from query response
	var access_token string
	rows.Next()
	err = rows.Scan(&access_token)
	if err != nil {
		return "", err
	}
	return access_token, nil
}

// GetUser returns the profile info of a user
func GetUser(email string) (*User, error) {
	query := fmt.Sprintf("SELECT id, fullname, address, email, telephone FROM user WHERE email = '%s';", email)
	rows, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return nil, err
	}

	// extract the user's info from query response
	user := &User{}
	rows.Next()
	err = rows.Scan(&user.ID, &user.FullName, &user.Address, &user.Email, &user.Telephone)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// CreateNewUser inserts a new User into the database
func CreateNewUser(user User) (*User, error) {
	// TODO store a hash of the password
	query := fmt.Sprintf("INSERT INTO user (fullname, address, telephone, email, password, access_token) VALUES ('%s', '', '', '%s', '%s', '%s');", user.FullName, user.Email, user.Password, user.AccessToken)
	_, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return nil, err
	}

	// we try to get the id of the user so we can use it for other requests
	query = fmt.Sprintf("SELECT id, email FROM user WHERE email = '%s' AND password = '%s';", user.Email, user.Password)
	rows, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return nil, err
	}

	// extract the user's info from query response
	response := &User{}
	rows.Next()
	err = rows.Scan(&response.ID, &response.Email)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// InsertResetToken is setting a new reset_token for a specific user
func InsertResetToken(email, resetToken string) error {
	// update information related fields of the user
	// also check if the user did not signed in using google auth
	query := fmt.Sprintf("UPDATE user SET reset_token='%s' WHERE email='%s' AND access_token='';", resetToken, email)
	_, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return err
	}
	return nil
}

// ResetPassword updates the password for a specific user and changes the resetToken for security reasons
func ResetPassword(rp *form.ResetPassword, randomResetToken string) error {
	// update information related fields of the user
	query := fmt.Sprintf("UPDATE user SET password = '%s', reset_token='%s' WHERE email='%s' AND reset_token = '%s'", rp.Password, randomResetToken, rp.Email, rp.ResetToken)
	_, err := dbmanager.GetDB().Query(query)
	if err != nil {
		return err
	}
	return nil
}
