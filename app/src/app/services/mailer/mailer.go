package mailer

import (
	"bytes"
	"log"
	"net/smtp"
	"sync"
)

const defaultSender = "test@demo.com"

// defaultMailerInstance is using the default email address and it's used as a sigleton
var defaultMailerInstance *Mailer
var once sync.Once

// InitDefaultMailer is instantiating the default mailer
func InitDefaultMailer(serverAddress, sender string) {
	once.Do(func() {
		defaultMailerInstance = NewMailer(serverAddress, sender)
	})
}

// SendMail cand be used to send mails using the default mailer
func SendMail(recipient, content string) error {
	return defaultMailerInstance.SendMail(recipient, content)
}

type Mailer struct {
	serverAddress string
	sender        string
}

func NewMailer(serverAddress, sender string) *Mailer {
	if len(sender) == 0 {
		sender = defaultSender
	}
	return &Mailer{
		serverAddress: serverAddress,
		sender:        sender,
	}
}

func (m *Mailer) SendMail(recipient, content string) error {
	// Connect to the remote SMTP server.
	c, err := smtp.Dial(m.serverAddress)
	if err != nil {
		log.Println(err)
		return err
	}

	// Set the sender and recipient first
	if err := c.Mail(m.sender); err != nil {
		log.Println(err)
		return err
	}
	if err := c.Rcpt(recipient); err != nil {
		log.Println(err)
		return err
	}

	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		log.Println(err)
		return err
	}
	defer wc.Close()
	buf := bytes.NewBufferString(content)
	if _, err = buf.WriteTo(wc); err != nil {
		log.Println(err)
	}
	return nil
}
