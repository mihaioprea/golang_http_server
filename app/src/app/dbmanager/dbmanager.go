package dbmanager

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

const defaultDatabase string = "app"

var db *sql.DB
var sqlDSN string

func GetDB() *sql.DB {
	if db == nil {
		// TODO handle the case when we can't establish an connection
		db, _ = getNewConnection()
	}
	return db
}

// getNeWConnection tries to establish a new connection to the mysql server
func getNewConnection() (*sql.DB, error) {
	db, err := sql.Open("mysql", sqlDSN)
	if err != nil {
		return nil, err
	}
	// test the connection
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}

// InitDb creates the required database and tables, it also returns a connection to the database
func InitDb(DSN string) (*sql.DB, error) {
	sqlDSN = DSN
	db, err := getNewConnection()
	if err != nil {
		return nil, err
	}

	// creating the database if it doesn't exist
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;", defaultDatabase))
	if err != nil {
		return nil, err
	}
	db.Close()

	// atach the database name to the DSN for further connection attempts
	sqlDSN = DSN + defaultDatabase
	// connect to the newly created database
	db, err = getNewConnection()
	if err != nil {
		return nil, err
	}

	// create the user table if it doesn't exist
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS user (id integer NOT NULL AUTO_INCREMENT, fullname varchar(32) NOT NULL, address varchar(256) NOT NULL, email varchar(32) NOT NULL, telephone varchar(32) NOT NULL, password varchar(32), access_token varchar(128), reset_token varchar(64), PRIMARY KEY (id), UNIQUE(email));")
	if err != nil {
		db.Close()
		return nil, err
	}

	log.Println("Database initialized")
	return db, nil
}
