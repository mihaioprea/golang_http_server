package http

import (
	"log"
	"net/http"
)

// AppHandler is to be used in error handling
type AppHandler func(http.ResponseWriter, *http.Request) *AppError

type AppError struct {
	Err     error
	Message string
	Code    int
}

// ServeHTTP formats and passes up an error
func (fn AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if e := fn(w, r); e != nil { // e is *appError, not os.Error.
		log.Println(e.Err)
		http.Error(w, e.Message, e.Code)
	}
}
